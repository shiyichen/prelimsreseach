___author__ = 'Shiyi'
# this will delete all non-ASCII characters
import mysql.connector
import re
import sys

cnx = mysql.connector.connect(user='root', password='jTGxXEdxwUdppbfp', database='stackoverflow')
cursor = cnx.cursor()

table = "stack5kview30k"
selectAmount = "50"
optionLimit = " LIMIT " + selectAmount
queryOne = "SELECT RowNum, ProcessedBody FROM " + table + optionLimit
queryTwo = "SELECT RowNum, ProcessedBody FROM " + table
#query = ("SELECT RowNum, Tags FROM stack50kview30k LIMIT " + selectAmount)
#query = ("SELECT RowNum, Tags FROM stack50kview30k")
cursor.execute(queryTwo)
#patten = re.compile('name=\w+;size=\d+', re.U)
patten = re.compile(r'[^\x00-\x7F]+')

updateList = []
updateQuery = ''

for (RowNum, ProcessedBody) in cursor:
    result = re.search(patten, ProcessedBody)
    if result is not None:
        #print(ProcessedBody)
        # delete all non-ascii chars
        newBody = ''.join(i for i in ProcessedBody if ord(i)<128)
        #print(newBody)
        updateQuery = "UPDATE " + table + " SET PBodyOnlyASCII=\"\"" + newBody + "\"\" WHERE RowNum = " + str(RowNum)
        newBody = ''
    else:
        updateQuery = "UPDATE " + table + " SET PBodyOnlyASCII=\"\"" + ProcessedBody + "\"\" WHERE RowNum = " + str(RowNum)
    #print(updateQuery)
    updateList.append(updateQuery)
    updateQuery = ''

#for query in updateList:
#    # print(query)
#    try:
#        cursor.execute(query)
#    except:
#        print(sys.exc_info()[0])
#        print(query)
cnx.commit()

cursor.close()
cnx.close()