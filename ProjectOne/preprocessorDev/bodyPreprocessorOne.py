import datetime
import mysql.connector
import re
from html.parser import HTMLParser
from bs4 import BeautifulSoup
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer

# split camel case
# http://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-camel-case
first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')
def convertCCS(name):
    s1 = first_cap_re.sub(r'\1 \2', name)
    return all_cap_re.sub(r'\1 \2', s1).lower()

# expand contractions
# http://stackoverflow.com/questions/19790188/expanding-english-language-contractions-in-python
contractions_dict = {
    "aren\'t": "are not",
    "can\'t": "can not",
    "'cause": "because",
    "could\'ve": "could have",
    "couldn\'t": "could not",
    "didn\'t": "did not",
    "doesn\'t": "does not",
    "don\'t": "do not",
    "hadn\'t": "had not",
    "hasn\'t": "has not",
    "haven\'t": "have not",
    "i\'d": "i would",
    "i\'ll": "i will",
    "i\'m": "i am",
    "i\'ve": "i have",
    "isn\'t": "is not",
    "it\'ll": "it will",
    "it\'s": "it is",
    "let's": "let us",
    "might\'ve": "might have",
    "must\'ve": "must have",
    "mustn\'t": "must not",
    "needn\'t": "need not",
    "o\'clock": "of the clock",
    "oughtn\'t": "ought not",
    "shan\'t": "shall not",
    "should\'ve": "should have",
    "shouldn\'t": "should not",
    "that\'d": "that would",
    "that\'s": "that is",
    "there\'s": "there is",
    "they\'d": "they would",
    "they\'ll": "they will",
    "they\'re": "they are",
    "they\'ve": "they have",
    "wasn\'t": "was not",
    "we\'d": "we would",
    "we\'ll": "we will",
    "we\'re": "we are",
    "we\'ve": "we have",
    "weren\'t": "were not",
    "what\'re": "what are",
    "what\'s": "what is",
    "where\'s": "where has / where is",
    "who\'ll": "who will",
    "who\'s": "who is",
    "why\'s": "why is",
    "will\'ve": "will have",
    "won\'t": "will not",
    "would\'ve": "would have",
    "wouldn\'t": "would not",
    "you\'d": "you would",
    "you\'ll": "you will",
    "you\'re": "you are",
    "you\'ve": "you have"
}
contractions_re = re.compile('(%s)' % '|'.join(contractions_dict.keys()))
print('(%s)' % '|'.join(contractions_dict.keys()))
def expand_contractions(s, contractions_dict=contractions_dict):
   def replace(match):
       return contractions_dict[match.group(0)]
   return contractions_re.sub(replace, s)

cnx = mysql.connector.connect(user='root', password='jTGxXEdxwUdppbfp', database='stackoverflow')
# cnx = mysql.connector.connect(user='root', password='jTGxXEdxwUdppbfp', database='shiyi')
cursor = cnx.cursor()

query = ("SELECT RowNum, Body FROM 50kview30k")
#query = ("SELECT RowNum, Body FROM stack50kview30k")

cursor.execute(query)
cCSBody = [] # camel case split & number removed
tokenizedBody = []
stopWordsRemoved = []
stemming = []
processedText = ''
ps = PorterStemmer()
updateList = []
updateQuery = ''

stop_words = set(stopwords.words("english"))
# for (RowNum, Tag, Body, ProcessedBody, ViewCount, Score) in cursor:
count = 0
for (RowNum, Body) in cursor:
    count += 1
    if (count % 500 == 0):
        print(count)
    #soup = BeautifulSoup(Body, 'xml') # can use it because lxml only avaliable for python 3.2, when pip install lxml
    # on python 3.4, you get errors.
    soup = BeautifulSoup(Body, 'html.parser')
    #print(Body)
    #print(soup.get_text())
    expandContractions = expand_contractions(soup.get_text().lower())
    #print(expandContractions)
    #tokenizedBody = word_tokenize(expandContractions)
    tokenizedBody = re.split("\W+", expandContractions)
    #print(tokenizedBody)
    #if RowNum == 129:
    #print(tokenizedBody)

    for w in tokenizedBody:
      if re.match(r'\w+[A-Z]\w+', w): # identify camel case method name
          result = convertCCS((w)).split()
          for i in result:
              cCSBody.append(i)
      elif re.match(r'[0-9]+', w): # number removing
          continue
      else:
          cCSBody.append(w)
    #print(cCSBody)
    for w in cCSBody: # stop words removing
      if w not in stop_words:
          stopWordsRemoved.append(w)
    #print(stopWordsRemoved)
    for w in stopWordsRemoved: # stemming and case lowering
      stemming.append(ps.stem(w))
    #print(stemming)
    for w in stemming:
      processedText += w
      processedText += ' '
    #print(processedText)
    updateQuery = "UPDATE stack50kview30k SET ProcessedBody=\"\"\"" + processedText + "\"\"\" WHERE RowNum=" + str(RowNum) # id is int, so might need to take out ' '
    #print(updateQuery)
    updateList.append(updateQuery)
    # clean all containers
    expandContractions = ''
    tokenizedBody = []
    cCSBody = []
    stopWordsRemoved = []
    stemming = []
    processedText = ''


# cursor.execute("""UPDATE idbody10 SET ProcessedBody='haha1' WHERE RowNum='104'""")
print("UPDATE START!")
count = 0
for query in updateList:
    # print(query)
    cursor.execute(query)
    count += 1
    if (count % 50 == 0):
        print(count)
cnx.commit()

cursor.close()
cnx.close()

