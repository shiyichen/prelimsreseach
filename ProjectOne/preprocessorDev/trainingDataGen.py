__author__ = 'Shiyi'
import mysql.connector
import re
import sys
import traceback
from collections import Counter

fileW1 = open("C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\inputGen50k100.dat", "w")
#fileR1 = open("C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\tags5k100.txt", "r")
fileR1 = open("C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\tags.txt", "r")
#fileW3 = open("C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\testData.dat", "w")

cnx = mysql.connector.connect(user='root', password='jTGxXEdxwUdppbfp', database='stackoverflow')
# cnx = mysql.connector.connect(user='root', password='jTGxXEdxwUdppbfp', database='shiyi')
cursor = cnx.cursor()

table = "stack50kview30k"
selectAmount = "3"
optionLimit = " LIMIT " + selectAmount
queryOne = "SELECT RowNum, KeyTags, ProcessedBody FROM " + table + optionLimit
#queryTwo = "SELECT RowNum, KeyTags, PBodyOnlyASCII FROM " + table
queryTwo = "SELECT RowNum, KeyTags, ProcessedBody FROM " + table
#query = ("SELECT RowNum, Tags FROM stack50kview30k LIMIT " + selectAmount)
#query = ("SELECT RowNum, Tags FROM stack50kview30k")
cursor.execute(queryTwo)

tagsCollection = []
tagSet = fileR1.read().splitlines()
print(tagSet)
inputGen = ''
bodyOnlyForTest = ''

patten = re.compile(r'[^\x00-\x7F]+')

count = 0
#for (RowNum, KeyTags, PBodyOnlyASCII) in cursor:
for (RowNum, KeyTags, ProcessedBody) in cursor:
    count += 1
    if (count % 1000 == 0):
        print(count)
    KeyTags = re.sub(r"\"", ">" , KeyTags)
    tagSub = '['
    addSpace = False
    #for tag in re.split(r">|<", KeyTags):
    #    if tag != '':
    #        print(tag)
    #        print(str(tagSet.index(tag)))
    try:
        for tag in re.split(r">|<", KeyTags):
            if tag != '' and addSpace:
                #print(tag)
                tagSub += ' '
                tagSub += str(tagSet.index(tag))
            if tag != '' and addSpace == False:
                #print(tag)
                tagSub += str(tagSet.index(tag))
                addSpace = True
        tagSub += ']'
        #print(tagSub)
        if tagSub != '[]':
            #tagPlusBody = tagSub +PBodyOnlyASCII
            tagPlusBody = tagSub + ProcessedBody
            tagPlusBody = re.sub(r"\"", " ", tagPlusBody)
            #result = re.search(patten, tagPlusBody)
            #if result is not None:
            #    print(tagPlusBody)
            #print(tagPlusBody)
            inputGen += tagPlusBody
            inputGen += '\n'
    except Exception:
        #print(RowNum)
        #print(KeyTags)
        print(sys.exc_info()[0])
        print(traceback.format_exc())

    #print(tagPlusBody)
    #print(tagSub)
    tagSub = ''
# print(inputGen)
fileW1.write(inputGen)

cursor.close()
cnx.close()
fileW1.close()
fileR1.close()
