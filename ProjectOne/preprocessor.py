__author__ = 'Shiyi'
import sys, traceback, re, mysql.connector, math, nltk, operator
from bs4 import BeautifulSoup
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from collections import Counter
from nltk.tag.stanford import StanfordPOSTagger

import os
java_path = "C:\\Program Files\\Java\\jdk1.8.0_20\\bin\\java.exe"
os.environ['JAVA_HOME']= java_path

#GLOBAL = [49, 10, 10, 2, 'N']
#GLOBAL = [169, 10, 10, 25, 'F']
GLOBAL = [438, 5, 5, 25, 'Rest']
# 1st: # of Tags
# 2nd: # of output from FIC/BIC
# 3rd: Recall @ K
# 4th: threshold
# 5th: F for FIC, B for BIC, E for EnTagRec, T for other tests

#by default: these options are all true. you can change one and only one to False at a time
EXPAND_CONTRACTION = True
STEM_BIC = True
NOT_STEM_FIC = True


username='root'
pw='jTGxXEdxwUdppbfp'
db='stackoverflow'

config = {
    'user': 'root',
    'password': 'jTGxXEdxwUdppbfp',
    'host': 'localhost',
    'database': 'stackoverflow',
}

#table = 'stack50k2008'
table = 'stack50kview30k_2'
#opt = 'WoStem'
#opt = 'FICth'
#opt = 'tag350'
#opt = 'tag257'
#opt = 'tag169'
#opt='tag100'
opt='tag438'
#opt = 'tag79'

table_opt = table + opt
print(table_opt)
#table = 'stack5kview30k_4'
#table = 'stack50kview30k_2' #remember to add a folder named after the table name @ LLDA/iofiles
                            #also REMEMBER to change topN
# key tag list file path
topN = GLOBAL[0] # top 49 tags for 5kview30k, 409 for 50kstack30k_2, 438 for 50k2008
            # different value for different table, find the value that can produce CONSISTENT top N tags
top_n = GLOBAL[1] # predict top 10 from BIC and FIC // set this = RECALLatK to test recall@k for BIC and FIC, but for EnTagRec, set it >=10, only RECALLatK is for recall@k
RECALLatK = GLOBAL[2] # recall @ k
tagCount = []
predictionOfFIC = []
predictionOfBIC = []
tagsListOfTuningdata = []
tagsListOfTestingData = []
keyTagFilePath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\keyTag" + str(topN) + ".txt"
restTagFilePath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\restTags" + str(topN) + ".txt"

# for trainingDataGen
trainingDataPathBIC = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\trainingDataBIC.dat"
trainingDataPathFIC = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\trainingDataFIC.dat"

trainingDataPathBICForRestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\trainingDataBICForRestTags.dat"
trainingDataPathFICForTestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\trainingDataFICForRestTags.dat"

# for testDataGenForBIC&FIC
testDataPathForBIC = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataForBIC.dat"
testDataPathForFIC = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataForFIC.dat"
testDataKeyTagsPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataKeyTags.txt"

testDataPathForBICForRestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataForBICForRestTags.dat"
testDataPathForFICForRestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataForFICForRestTags.dat"
testDataKeyTagsPathForRestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataKeyTagsForRestTags.txt"

# for testDataGenForTuning
testDataPathForTuning = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataForTuning.dat"
testDataKeyTagsPathForTuning = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\testDataKeyTagsForTuning.txt"
FICtopKPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\FICtopK.txt"
FICtopKValuePath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\FICtopKvalue.txt"
BICtopKPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\BICtopK.txt"
BICtopKvaluePath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\BICtopKvalue.txt"
FinaltopKPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\FinaltopK.txt"

# for words of tag
wordOfTagPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\wordOfTag.txt"
# for generating matrix of weight (SA)
matrixOfWeight = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\matrixOfWeight.txt"

# for output of BIC:
#outputOfBICPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table + "\\testDataForBIC.dat.t50n500w200.theta"
outputOfBICPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\outputOfBIC.theta"
topTagsPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\topTags.txt"
cat1Path = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\cat1_PL.txt"
cat2Path = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\cat2_FW.txt"
cat3Path = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\cat3_DB.txt"
cat4Path = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\cat4_OS.txt"


outputOfBICPathForRestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\outputOfBICForRestTags.theta"
BICtopKPathForRestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\BICtopKForRestTags.txt"
BICtopKValuePathForRestTags = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\" + table_opt + "\\BICtopKvalueForRestTags.txt"

def bodyProcessor():
    print("Calling bodyProcessor():")
    # split camel case
    # http://stackoverflow.com/questions/1175208/elegant-python-function-to-convert-camelcase-to-camel-case
    first_cap_re = re.compile('(.)([A-Z][a-z]+)')
    all_cap_re = re.compile('([a-z0-9])([A-Z])')
    def convertCCS(name):
        s1 = first_cap_re.sub(r'\1 \2', name)
        return all_cap_re.sub(r'\1 \2', s1).lower()

    # expand contractions
    # http://stackoverflow.com/questions/19790188/expanding-english-language-contractions-in-python
    contractions_dict = {
        "aren\'t": "are not",
        "can\'t": "can not",
        "'cause": "because",
        "could\'ve": "could have",
        "couldn\'t": "could not",
        "didn\'t": "did not",
        "doesn\'t": "does not",
        "don\'t": "do not",
        "hadn\'t": "had not",
        "hasn\'t": "has not",
        "haven\'t": "have not",
        "i\'d": "i would",
        "i\'ll": "i will",
        "i\'m": "i am",
        "i\'ve": "i have",
        "isn\'t": "is not",
        "it\'ll": "it will",
        "it\'s": "it is",
        "let's": "let us",
        "might\'ve": "might have",
        "must\'ve": "must have",
        "mustn\'t": "must not",
        "needn\'t": "need not",
        "o\'clock": "of the clock",
        "oughtn\'t": "ought not",
        "shan\'t": "shall not",
        "should\'ve": "should have",
        "shouldn\'t": "should not",
        "that\'d": "that would",
        "that\'s": "that is",
        "there\'s": "there is",
        "they\'d": "they would",
        "they\'ll": "they will",
        "they\'re": "they are",
        "they\'ve": "they have",
        "wasn\'t": "was not",
        "we\'d": "we would",
        "we\'ll": "we will",
        "we\'re": "we are",
        "we\'ve": "we have",
        "weren\'t": "were not",
        "what\'re": "what are",
        "what\'s": "what is",
        "where\'s": "where has / where is",
        "who\'ll": "who will",
        "who\'s": "who is",
        "why\'s": "why is",
        "will\'ve": "will have",
        "won\'t": "will not",
        "would\'ve": "would have",
        "wouldn\'t": "would not",
        "you\'d": "you would",
        "you\'ll": "you will",
        "you\'re": "you are",
        "you\'ve": "you have"
    }
    contractions_re = re.compile('(%s)' % '|'.join(contractions_dict.keys()))
    #print('(%s)' % '|'.join(contractions_dict.keys()))
    def expand_contractions(s, contractions_dict=contractions_dict):
       def replace(match):
           return contractions_dict[match.group(0)]
       return contractions_re.sub(replace, s)

    cnx = mysql.connector.connect(**config)
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)

    query = "SELECT RowNum, Body FROM " + table# + " LIMIT 3"
    #query = ("SELECT RowNum, Body FROM stack50kview30k")

    cursor.execute(query)
    cCSBody = [] # camel case split & number removed
    tokenizedBody = []
    stopWordsRemoved = []
    stemming = []
    processedText = ''
    ps = PorterStemmer()
    updateList = []
    updateQuery = ''

    stop_words = set(stopwords.words("english"))
    # for (RowNum, Tag, Body, ProcessedBody, ViewCount, Score) in cursor:
    print("Processing Body: ")
    count = 0
    i = 1
    currentRow = oneTenth
    for (RowNum, Body) in cursor:
        count += 1
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        #soup = BeautifulSoup(Body, 'xml') # can use it because lxml only avaliable for python 3.2, when pip install lxml
        # on python 3.4, you get errors.
        soup = BeautifulSoup(Body, 'html.parser')
        #print(Body)
        #print(soup.get_text())
        #print("-----------------------------------")
        if EXPAND_CONTRACTION:
            expandContractions = expand_contractions(soup.get_text().lower())
        else:
            expandContractions = soup.get_text().lower()
        #print(expandContractions)
        #tokenizedBody = word_tokenize(expandContractions)
        tokenizedBody = re.split("\W+", expandContractions)
        #print(tokenizedBody)

        for w in tokenizedBody:
          if re.match(r'\w+[A-Z]\w+', w): # identify camel case method name
              result = convertCCS((w)).split()
              for i in result:
                  cCSBody.append(i)
          elif re.match(r'[0-9]+', w): # number removing
              continue
          else:
              cCSBody.append(w)
        #print(cCSBody)
        for w in cCSBody: # stop words removing
          if w not in stop_words:
              stopWordsRemoved.append(w)
        #print(stopWordsRemoved)
        for w in stopWordsRemoved: # stemming and case lowering
            if STEM_BIC:
                stemming.append(ps.stem(w))
            else:
                stemming.append(w)
        #print(stemming)
        for w in stemming:
          processedText += w
          processedText += ' '
        #print(processedText)
        if EXPAND_CONTRACTION and STEM_BIC:
            updateQuery = "UPDATE " + table + " SET ProcessedBody=\"\"\"" + processedText + "\"\"\" WHERE RowNum=" + str(RowNum) # id is int, so might need to take out ' '
        elif EXPAND_CONTRACTION is False:
            updateQuery = "UPDATE " + table + " SET PBodyWoContraction=\"\"\"" + processedText + "\"\"\" WHERE RowNum=" + str(RowNum) # id is int, so might need to take out ' '
        elif STEM_BIC is False:
            updateQuery = "UPDATE " + table + " SET PBodyWoStem=\"\"\"" + processedText + "\"\"\" WHERE RowNum=" + str(RowNum) # id is int, so might need to take out ' '

        #print(updateQuery)
        updateList.append(updateQuery)
        # clean all containers
        expandContractions = ''
        tokenizedBody = []
        cCSBody = []
        stopWordsRemoved = []
        stemming = []
        processedText = ''


    # cursor.execute("""UPDATE idbody10 SET ProcessedBody='haha1' WHERE RowNum='104'""")
    print("\nUpdating table")
    count = 0
    i = 1
    currentRow = oneTenth
    for query in updateList:
        #print(query)
        cursor.execute(query)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
    cnx.commit()

    cursor.close()
    cnx.close()

def keepOnlyNoun():
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)
    oneHundredth = math.floor(totalrow / 100)

    query = "SELECT RowNum, Body FROM " + table# + " LIMIT 10" #+ " WHERE RowNum = 23"
    cursor.execute(query)
    list_nouns = []
    stemmed_nouns = []
    only_nouns = ''
    updateList = []
    updateQuery = ''
    count = 0
    i = 1
    currentRow = oneHundredth
    print('\nPOS tagging and filtering...(this might take a long time)')
    wordPattern = re.compile("^[a-z]+$")
    for (RowNum, Body) in cursor:
        soup = BeautifulSoup(Body, 'html.parser')
        text = soup.get_text().lower()
        #print(text)
        text = word_tokenize(text)
        text = nltk.pos_tag(text, tagset='universal')
        #print(text)
        for item in text:
            if item[1] == 'NOUN': #and wordPattern.match(item[0]):
                list_nouns.append(item[0])
        #set(list_nouns)
        #print(list_nouns)
        #for item in list_nouns:
        #    stemmed_nouns.append(PorterStemmer().stem(item))

        #for w in list_nouns:
        for w in set(list_nouns):
            only_nouns += w
            only_nouns += ' '

        updateQuery = "UPDATE " + table + " SET OnlyNounBody=\"\"\"" + only_nouns + "\"\"\" WHERE RowNum=" + str(RowNum)
        #print(updateQuery)
        updateList.append(updateQuery)

        list_nouns = []
        stemmed_nouns = []
        only_nouns = ''
        updateQuery = ''
        #print(count, end=' ', flush=True)
        if (count == currentRow):
            print(i, "%", end="|", flush=True)
            currentRow += oneHundredth
            i+=1
        count += 1

    #print(list_nouns)
    #print(stemmed_nouns)

    print("\nUpdating the OnlyNounBody column")
    count = 0
    i = 1
    currentRow = oneTenth
    for query in updateList:
        #print(query)
        cursor.execute(query)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
    cnx.commit()

    cursor.close()
    cnx.close()

def keyTagListGen():
    print("\n\nCalling KeyTagListGen():")
    fileW = open(keyTagFilePath, "w")
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    query = "SELECT RowNum, Tags FROM " + table
    cursor.execute(query)
    tagsCollection = []
    tagSet = []
    # for (RowNum, Tag, Body, ProcessedBody, ViewCount, Score) in cursor:
    count = 0
    for (RowNum, Tags) in cursor:
        count += 1
        #print(Tags)
        # print(re.split("\W+", Tags))
        # print(re.findall(r"<\w+>", Tags))
        try:
            tagsplit = re.split(r">|<", Tags)
        except:
            print(RowNum)
            print(Tags)
        for tag in tagsplit:
            if tag != '':
                tagsCollection.append(tag)
    # print(tagsCollection)
    counter = Counter(tagsCollection).most_common(topN)
    print("Top", topN, "tags identified" )
    print(counter)
    for i in range(0, len(counter)):
        tagSet.append(counter[i][0])
        tagCount.append(counter[i][1])
    #print(tagCount)
    # tagSet = list(set(tagsCollection))
    # tagSet.sort()
    for i in range(0, len(tagSet)):
        #fileW.write(tagSet[i] + '\n')
        fileW.write(str(tagCount[i]) + '\n')
    #print(tagSet)
    # too many tags !! for 5k posts, there are 2568 different tags!!!!
    cursor.close()
    cnx.close()
    fileW.close()

def keyTagsWriteToTable():
    print("\nCalling keyTagsWriteToTable():")
    fileR = open(keyTagFilePath, "r")
    keyTagCollection = fileR.read().splitlines()

    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)

    query = "SELECT RowNum, Tags FROM " + table
    cursor.execute(query)

    keyTagForTheRow = ''
    updateList = []

    count = 0
    currentRow = oneTenth
    i = 1
    print("Filtering Tags: ")
    for RowNum, Tags in cursor:
        #print(Tags)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
        try:
            tagsplit = re.split(r">|<", Tags)
            #print(tagsplit)
        except:
            print("exception: " + str(Tags))
        for tag in tagsplit:
            if tag != '' and tag in keyTagCollection:
                keyTagForTheRow += '<'
                keyTagForTheRow += tag
                keyTagForTheRow += '>'
        #print(keyTagForTheRow)
        updateQuery = "UPDATE " + table + " SET KeyTags=\"\"\"" + keyTagForTheRow + "\"\"\" WHERE RowNum = " + str(RowNum)
        #print(updateQuery)
        updateList.append(updateQuery)
        updateQuery = ''
        keyTagForTheRow = ''


    count = 0
    i = 1
    currentRow = oneTenth
    print("\nInserting KeyTags: ")
    for q in updateList:
        # print(query)
        cursor.execute(q)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
    cnx.commit()

    cursor.close()
    cnx.close()

def removeEmptyKeyTagRow():
    print("\nRemoving rows with no key tags")
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
    query = "SELECT RowNum, KeyTags FROM " + table
    cursor.execute(query)

    updateList = []
    for RowNum, KeyTags in cursor:
        if KeyTags == "\"\"":
            #print(KeyTags)
            #Next time, directly delete. (about 24 rows)
            updateQuery = "DELETE FROM " + table + " WHERE RowNum = " + str(RowNum)
            updateList.append(updateQuery)
            updateQuery = ''

    for q in updateList:
        cursor.execute(q)
    print(len(updateList), "row deleted")
    cnx.commit()

    cursor.close()
    cnx.close()

def userTagsFilterOutTopTags(updateTable, writeToFile):
    print("\n\nFiltering top tags from user tags():")
    fileR = open(topTagsPath, "r")
    fileW = open(restTagFilePath, "w")
    topTagCollection = set(fileR.read().splitlines())
    tagCountList = [0,0,0,0,0,0,0,0,0,0,0]
    restTagsList = []
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)

    query = "SELECT RowNum, KeyTags FROM " + table
    cursor.execute(query)

    keyTagForTheRow = ''
    updateList = []

    count = 0
    currentRow = oneTenth
    i = 1
    print("Filtering Tags: ")
    for RowNum, Tags in cursor:
        tempCount = 0
        #print(Tags)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
        try:
            tagsplit = re.split(r">|<", Tags)
            #print(tagsplit)
        except:
            print("exception: " + str(Tags))
        for tag in tagsplit:
            if tag != '' and tag != "\"" and tag != "<" and tag != ">" and tag not in topTagCollection:
                tempCount+=1
                restTagsList.append(tag)
                keyTagForTheRow += '<'
                keyTagForTheRow += tag
                keyTagForTheRow += '>'
        #print(keyTagForTheRow)
        if keyTagForTheRow != "":
            updateQuery = "UPDATE " + table + " SET TopTagsGone=\"\"\"" + keyTagForTheRow + "\"\"\" WHERE RowNum = " + str(RowNum)
        else:
            updateQuery = "UPDATE " + table + " SET TopTagsGone=\"\"\"" + '' + "\"\"\" WHERE RowNum = " + str(RowNum)
        #print(updateQuery)
        updateList.append(updateQuery)
        updateQuery = ''
        keyTagForTheRow = ''

        tagCountList[tempCount] += 1

    i = 1
    count = 0
    currentRow = oneTenth
    if updateTable:
        print("\nInserting TopTagsGone: ")
        print(str(len(updateList)) + " lines will be updated.")
        for q in updateList:
            # print(query)
            cursor.execute(q)
            if (count == currentRow):
                print(i*10, "%", end="|", flush=True)
                currentRow += oneTenth
                i+=1
            count += 1
        cnx.commit()
        print()

    count = 0
    for x in range(len(tagCountList)):
        count += tagCountList[x]
    print("count: " + str(count))
    for x in range(len(tagCountList)):
        tagCountList[x] = tagCountList[x]/count*100

    print(tagCountList)

    if writeToFile:
        restTagsList = list(set(restTagsList))
        for x in range(len(restTagsList)):
            fileW.write(restTagsList[x] + '\n')
        print("restTags file created")
    cursor.close()
    cnx.close()
    fileR.close()
    fileW.close()

def checkIfInCategories():
    print("\n\nCheck if in top categories:")
    fileR1 = open(cat1Path, "r")
    fileR2 = open(cat2Path, "r")
    fileR3 = open(cat3Path, "r")
    fileR4 = open(cat4Path, "r")
    cat1 = set(fileR1.read().splitlines())
    cat2 = set(fileR2.read().splitlines())
    cat3 = set(fileR3.read().splitlines())
    cat4 = set(fileR4.read().splitlines())
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)

    query = "SELECT RowNum, Tags FROM " + table
    cursor.execute(query)

    updateList = []

    count = 0
    currentRow = oneTenth
    i = 1
    print("Filtering Tags: ")
    for RowNum, Tags in cursor:
        catCheckList = [0,0,0,0]
        #print(Tags)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
        try:
            tagsplit = re.split(r">|<", Tags)
            #print(tagsplit)
        except:
            print("exception: " + str(Tags))
        for tag in tagsplit:
            if tag != '':
                if tag in cat1:
                    catCheckList[0]=1
                elif tag in cat2:
                    catCheckList[1]=1
                elif tag in cat3:
                    catCheckList[2]=1
                elif tag in cat4:
                    catCheckList[3]=1
        #print(keyTagForTheRow)
        #updateQuery = "UPDATE " + table + " SET PL=\"\"\"" + str(catCheckList[0]+ "\"\"\" WHERE RowNum = " + str(RowNum)
        updateQuery = "UPDATE " + table + " SET PL=" + str(catCheckList[0]) + ", FW=" + str(catCheckList[1]) + ", DB=" + str(catCheckList[2]) + ", OS=" + str(catCheckList[3]) + " WHERE RowNum = " + str(RowNum)
        if count == 1:
            print(updateQuery)
        updateList.append(updateQuery)
        updateQuery = ''


    count = 0
    i = 1
    currentRow = oneTenth
    print("\nUpdating Categories: ")
    for q in updateList:
        # print(query)
        cursor.execute(q)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
    cnx.commit()

    print()
    cursor.close()
    cnx.close()
    fileR1.close()
    fileR2.close()
    fileR3.close()
    fileR4.close()
def stemNounsForFIC():
    print("\nStemming nouns for a FIC option")
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    query = "SELECT RowNum, OnlyNounBody FROM " + table# + " LIMIT 1"
    cursor.execute(query)
    updateList = []
    for (RowNum, OnlyNounBody) in cursor:
        try:
            stemmedNounString = ""
            for noun in re.split(" |\"", OnlyNounBody):
                if noun != '':
                    tempNoun = PorterStemmer().stem(noun)
                    stemmedNounString += tempNoun
                    stemmedNounString += " "
            updateQuery = "UPDATE " + table + " SET OnlyNounBodyWiStem=\"\"\"" + stemmedNounString + "\"\"\" WHERE RowNum = " + str(RowNum)
            updateList.append(updateQuery)
            updateQuery = ''
        except Exception:
            print(sys.exc_info()[0])
            print(traceback.format_exc())

    for q in updateList:
        cursor.execute(q)
    print(len(updateList), "rows of OnlyNounBodyWiStem updated")
    cnx.commit()

    cursor.close()
    cnx.close()

def filterBadNouns(threshold):

    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    if NOT_STEM_FIC:
        query = "SELECT RowNum, OnlyNounBody FROM " + table# + " LIMIT 1"
    else:
        query = "SELECT RowNum, OnlyNounBodyWiStem FROM " + table# + " LIMIT 1"
    cursor.execute(query)
    nounMap = {}
    for (RowNum, OnlyNounBody) in cursor:
        try:
            for noun in re.split(" |\"", OnlyNounBody):
                if noun != '':
                    tempNoun = noun
                    #print(tempNoun)
                    #print(noun)
                    if tempNoun not in nounMap:
                        nounMap[tempNoun] = 1
                    else:
                        nounMap[tempNoun] += 1
        except Exception:
            print(sys.exc_info()[0])
            print(traceback.format_exc())
    #print(len(nounMap))
    goodNounList = []
    for key, value in nounMap.items():
        if value > threshold:
            goodNounList.append(key)
    #print(len(goodNounList))
    goodNounSet = set(goodNounList)
    #print(len(goodNounSet))
    #print(goodNounSet)

    if NOT_STEM_FIC:
        query = "SELECT RowNum, OnlyNounBody FROM " + table# + " LIMIT 1"
    else:
        query = "SELECT RowNum, OnlyNounBodyWiStem FROM " + table# + " LIMIT 1"
    cursor.execute(query)
    updateList = []
    numOfDeletedRow = 0
    for (RowNum, OnlyNounBody) in cursor:
        try:
            goodNounString = ""
            noNounInGoodNounSet = True
            for noun in re.split(" |\"", OnlyNounBody):
                if noun != '':
                    #print(noun)
                    if noun in goodNounSet:
                        noNounInGoodNounSet = False
                        goodNounString += noun
                        goodNounString += " "

            if noNounInGoodNounSet:
                #updateQuery = "DELETE FROM " + table + " WHERE RowNum = " + str(RowNum)
                # if you don't want to delete any row because of this, comment line above and uncomment these two lines
                goodNounString = "java "
                if NOT_STEM_FIC:
                    updateQuery = "UPDATE " + table + " SET GoodNouns=\"\"\"" + goodNounString + "\"\"\" WHERE RowNum = " + str(RowNum)
                if NOT_STEM_FIC is False:
                    updateQuery = "UPDATE " + table + " SET GoodNounsWiStem=\"\"\"" + goodNounString + "\"\"\" WHERE RowNum = " + str(RowNum)
                numOfDeletedRow += 1
            else:
                if NOT_STEM_FIC:
                    updateQuery = "UPDATE " + table + " SET GoodNouns=\"\"\"" + goodNounString + "\"\"\" WHERE RowNum = " + str(RowNum)
                if NOT_STEM_FIC is False:
                    updateQuery = "UPDATE " + table + " SET GoodNounsWiStem=\"\"\"" + goodNounString + "\"\"\" WHERE RowNum = " + str(RowNum)
            #print(updateQuery)
            updateList.append(updateQuery)
        except Exception:
            print(sys.exc_info()[0])
            print(traceback.format_exc())

    for q in updateList:
        cursor.execute(q)
    cnx.commit()
    print(str(numOfDeletedRow) + " rows deleted because they don't have any noun in the good noun Set")
    cursor.close()
    cnx.close()

def trainingDataGen(BIC, FIC):
    print("\nCalling trainingDataGen()")
    fileWBIC = open(trainingDataPathBIC, "w", encoding='utf-8')
    fileWFIC = open(trainingDataPathFIC, "w", encoding='utf-8')
    fileR = open(keyTagFilePath, "r")

    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)

    #query = "SELECT RowNum, KeyTags, ProcessedBody, OnlyNounBody FROM " + table# + " LIMIT 100"
    if EXPAND_CONTRACTION:
        query = "SELECT RowNum, KeyTags, ProcessedBody, GoodNouns FROM " + table + " LIMIT " + str(oneTenth) + ", 999999999"
    else:
        query = "SELECT RowNum, KeyTags, PBodyWoContraction, GoodNouns FROM " + table + " LIMIT " + str(oneTenth) + ", 999999999"
    #print(query)
    cursor.execute(query)
    oneTenth = math.floor(oneTenth*0.9) # 10 fold cross validation

    tagsCollection = []
    tagSet = fileR.read().splitlines()
    #print(tagSet)
    inputGenBIC = ''
    inputGenFIC = ''
    #patten = re.compile(r'[^\x00-\x7F]+')
    print("Generating Training Data")
    count = 1
    i = 1
    currentRow = oneTenth
    #for (RowNum, KeyTags, PBodyOnlyASCII) in cursor:
    for (RowNum, KeyTags, ProcessedBody, GoodNouns) in cursor:
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
        KeyTags = re.sub(r"\"", ">" , KeyTags)
        tagSub = '['
        addSpace = False
        #for tag in re.split(r">|<", KeyTags):
        #    if tag != '':
        #        print(tag)
        #        print(str(tagSet.index(tag)))
        try:
            for tag in re.split(r">|<", KeyTags):
                if tag != '' and addSpace:
                    #print(tag)
                    tagSub += ' '
                    tagSub += str(tagSet.index(tag))
                if tag != '' and addSpace == False:
                    #print(tag)
                    tagSub += str(tagSet.index(tag))
                    addSpace = True
            tagSub += ']'
            #print(tagSub)
            if tagSub != '[]':
                #tagPlusBody = tagSub +PBodyOnlyASCII
                tagPlusBody = tagSub + ProcessedBody
                tagPlusBody = re.sub(r"\"", " ", tagPlusBody)
                #result = re.search(patten, tagPlusBody)
                #if result is not None:
                #    print(tagPlusBody)
                #print(tagPlusBody)
                inputGenBIC += tagPlusBody
                inputGenBIC += '\n'

                tagPlusOnlyNounBody = tagSub + GoodNouns
                tagPlusOnlyNounBody = re.sub(r"\"", " ", tagPlusOnlyNounBody)
                inputGenFIC += tagPlusOnlyNounBody
                inputGenFIC += '\n'
        except Exception:
            #print(RowNum)
            #print(KeyTags)
            print(count)
            print(sys.exc_info()[0])
            print(traceback.format_exc())

        #print(tagPlusBody)
        #print(tagSub)
        tagSub = ''
    # print(inputGen)
    print("\nWriting to training dat file")
    fileWBIC.write(inputGenBIC)
    fileWFIC.write(inputGenFIC)
    cursor.close()
    cnx.close()
    fileWBIC.close()
    fileWFIC.close()
    fileR.close()

def trainingDataGenForRestTags(BIC, FIC):
    print("\nCalling trainingDataGenForRestTags()")
    fileWBIC = open(trainingDataPathBICForRestTags, "w", encoding='utf-8')
    fileWFIC = open(trainingDataPathFICForTestTags, "w", encoding='utf-8')
    fileR = open(restTagFilePath, "r")

    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)

    #query = "SELECT RowNum, KeyTags, ProcessedBody, OnlyNounBody FROM " + table# + " LIMIT 100"
    if EXPAND_CONTRACTION:
        query = "SELECT RowNum, TopTagsGone, ProcessedBody, GoodNouns FROM " + table + " LIMIT " + str(oneTenth) + ", 999999999"
    else:
        query = "SELECT RowNum, TopTagsGone, PBodyWoContraction, GoodNouns FROM " + table + " LIMIT " + str(oneTenth) + ", 999999999"
    #print(query)
    cursor.execute(query)
    oneTenth = math.floor(oneTenth*0.9) # 10 fold cross validation

    tagsCollection = []
    tagSet = fileR.read().splitlines()
    #print(tagSet)
    inputGenBIC = ''
    inputGenFIC = ''
    #patten = re.compile(r'[^\x00-\x7F]+')
    print("Generating Training Data")
    count = 1
    i = 1
    currentRow = oneTenth
    #for (RowNum, KeyTags, PBodyOnlyASCII) in cursor:
    for (RowNum, KeyTags, ProcessedBody, GoodNouns) in cursor:
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
        KeyTags = re.sub(r"\"", ">" , KeyTags)
        tagSub = '['
        addSpace = False
        #for tag in re.split(r">|<", KeyTags):
        #    if tag != '':
        #        print(tag)
        #        print(str(tagSet.index(tag)))
        try:
            for tag in re.split(r">|<", KeyTags):
                if tag != '' and addSpace:
                    #print(tag)
                    tagSub += ' '
                    tagSub += str(tagSet.index(tag))
                if tag != '' and addSpace == False:
                    #print(tag)
                    tagSub += str(tagSet.index(tag))
                    addSpace = True
            tagSub += ']'
            #print(tagSub)
            if tagSub != '[]':
                #tagPlusBody = tagSub +PBodyOnlyASCII
                tagPlusBody = tagSub + ProcessedBody
                tagPlusBody = re.sub(r"\"", " ", tagPlusBody)
                #result = re.search(patten, tagPlusBody)
                #if result is not None:
                #    print(tagPlusBody)
                #print(tagPlusBody)
                inputGenBIC += tagPlusBody
                inputGenBIC += '\n'

                tagPlusOnlyNounBody = tagSub + GoodNouns
                tagPlusOnlyNounBody = re.sub(r"\"", " ", tagPlusOnlyNounBody)
                inputGenFIC += tagPlusOnlyNounBody
                inputGenFIC += '\n'
        except Exception:
            #print(RowNum)
            #print(KeyTags)
            print(count)
            print(sys.exc_info()[0])
            print(traceback.format_exc())

        #print(tagPlusBody)
        #print(tagSub)
        tagSub = ''
    # print(inputGen)
    print("\nWriting to training dat file (for rest tags)")
    fileWBIC.write(inputGenBIC)
    fileWFIC.write(inputGenFIC)
    cursor.close()
    cnx.close()
    fileWBIC.close()
    fileWFIC.close()
    fileR.close()

def testDataGenForBICandFICForRestTags():
    print("\nGenerating Testing Data for BIC and FIC")
    fileWBIC = open(testDataPathForBICForRestTags, "w", encoding='utf-8')
    fileWFIC = open(testDataPathForFICForRestTags, "w", encoding='utf-8')
    fileW2 = open(testDataKeyTagsPathForRestTags, "w")
    cnx = mysql.connector.connect(**config)
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cursor = cnx.cursor()
    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)
    query = "SELECT RowNum, TopTagsGone, ProcessedBody, GoodNouns FROM " + table + " LIMIT " + str(oneTenth)
    cursor.execute(query)
    bodyOnlyForTest = ''
    goodNounForTest = ''
    testDataKeyTags = ''
    for (RowNum, KeyTags, ProcessedBody, GoodNouns) in cursor:
        try:
            bodyOnlyForTest += ProcessedBody
            bodyOnlyForTest = re.sub(r"\"", " ", bodyOnlyForTest)
            bodyOnlyForTest += '\n'

            goodNounForTest += GoodNouns
            goodNounForTest = re.sub(r"\"", " ", goodNounForTest)
            goodNounForTest += '\n'

            testDataKeyTags += KeyTags
            testDataKeyTags += '\n'
        except Exception:
            print(RowNum)


    #print(bodyOnlyForTest)
    #print(testDataKeyTags)
    fileWBIC.write(bodyOnlyForTest)
    fileWFIC.write(goodNounForTest)
    fileW2.write(testDataKeyTags)
    cursor.close()
    cnx.close()
    fileWBIC.close()
    fileWFIC.close()
    fileW2.close()

def testDataGenForBICandFIC():
    print("\nGenerating Testing Data for BIC and FIC")
    fileWBIC = open(testDataPathForBIC, "w", encoding='utf-8')
    fileWFIC = open(testDataPathForFIC, "w", encoding='utf-8')
    fileW2 = open(testDataKeyTagsPath, "w")
    cnx = mysql.connector.connect(**config)
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cursor = cnx.cursor()
    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)
    if EXPAND_CONTRACTION and NOT_STEM_FIC:
        query = "SELECT RowNum, KeyTags, ProcessedBody, GoodNouns FROM " + table + " LIMIT " + str(oneTenth)
    elif EXPAND_CONTRACTION is False and NOT_STEM_FIC:
        query = "SELECT RowNum, KeyTags, PBodyWoContraction, GoodNouns FROM " + table + " LIMIT " + str(oneTenth)
    elif NOT_STEM_FIC is False and EXPAND_CONTRACTION:
        query = "SELECT RowNum, KeyTags, ProcessedBody, GoodNounsWiStem FROM " + table + " LIMIT " + str(oneTenth)
    else:
        print("Error: more than one options are set to False")
        exit(1)
    print(query)

    cursor.execute(query)
    bodyOnlyForTest = ''
    goodNounForTest = ''
    testDataKeyTags = ''
    for (RowNum, KeyTags, ProcessedBody, GoodNouns) in cursor:
        try:
            bodyOnlyForTest += ProcessedBody
            bodyOnlyForTest = re.sub(r"\"", " ", bodyOnlyForTest)
            bodyOnlyForTest += '\n'

            goodNounForTest += GoodNouns
            goodNounForTest = re.sub(r"\"", " ", goodNounForTest)
            goodNounForTest += '\n'

            testDataKeyTags += KeyTags
            testDataKeyTags += '\n'
        except Exception:
            print(RowNum)


    #print(bodyOnlyForTest)
    #print(testDataKeyTags)
    fileWBIC.write(bodyOnlyForTest)
    fileWFIC.write(goodNounForTest)
    fileW2.write(testDataKeyTags)
    cursor.close()
    cnx.close()
    fileWBIC.close()
    fileWFIC.close()
    fileW2.close()

def tuningDataSetGen():
    print("\nGenerating data for tuning")
    fileW = open(testDataPathForTuning, "w", encoding='utf-8')
    fileW2 = open(testDataKeyTagsPathForTuning, "w")
    cnx = mysql.connector.connect(**config)
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cursor = cnx.cursor()
    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneHundred= math.floor(totalrow / 100)
    oneThousandth = math.floor(totalrow / 1000)
    print("Tuning size in tuningDataSetGen:")
    print(oneHundred)
    print(oneThousandth)
    #query = "SELECT RowNum, KeyTags, GoodNouns FROM " + table + " LIMIT " + str(oneHundred)
    query = "SELECT RowNum, KeyTags, GoodNouns FROM " + table + " LIMIT " + str(oneThousandth)
    cursor.execute(query)
    bodyOnlyForTest = ''
    testDataKeyTagsForTuning = ''
    for (RowNum, KeyTags, GoodNouns) in cursor:
        bodyOnlyForTest += GoodNouns
        bodyOnlyForTest = re.sub(r"\"", " ", bodyOnlyForTest)
        bodyOnlyForTest += '\n'

        testDataKeyTagsForTuning += KeyTags
        testDataKeyTagsForTuning += '\n'

    #print(bodyOnlyForTest)
    #print(testDataKeyTags)
    fileW.write(bodyOnlyForTest)
    fileW2.write(testDataKeyTagsForTuning)
    cursor.close()
    cnx.close()
    fileW.close()
    fileW2.close()

def wordListForTag(): # this method generates training data for FIC (FrePOS)
    print("\nCalling wordListForTag()")
    fileW = open(wordOfTagPath, "w")
    fileR = open(keyTagFilePath, "r")
    tagSet = fileR.read().splitlines()
    n = len(tagSet)

    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)
    print(oneTenth)

    if NOT_STEM_FIC:
        query = "SELECT RowNum, KeyTags, GoodNouns FROM " + table# + " LIMIT " + str(oneTenth) + ", 999999999"
    else:
        query = "SELECT RowNum, KeyTags, GoodNounsWiStem FROM " + table# + " LIMIT " + str(oneTenth) + ", 999999999"

    cursor.execute(query)
    #oneTenth = math.floor(oneTenth*0.9) # 10 fold cross validation

    #initialize the collection
    wordOfTagCollection = []
    for x in range(n):
        aList = [str(x)]
        wordOfTagCollection.append(aList)

    count = 1
    i = 1
    currentRow = oneTenth
    j = 0
    for (RowNum, KeyTags, GoodNouns) in cursor:
        try:
            #text = word_tokenize(GoodNouns)
            #print(text)
            for tag in re.split(r">|<", KeyTags):
                #print(tag)
                if tag != '"' and tag != '':
                    index = tagSet.index(tag)
                    #for word in text:
                    for noun in re.split(r" |\"", GoodNouns):
                        if noun != " " and noun != "":
                            #print(noun)
                            wordOfTagCollection[index].append(noun)
        except Exception:
            print(sys.exc_info()[0])
            print(traceback.format_exc())

    print("Finish constructing words of tags, now writing to file " + wordOfTagPath)
    wordString = ''
    for x in range(n):
        #wordOfTagCollection[x] = sorted(set(wordOfTagCollection[x])) # set filter out duplicated elements in a list, sort() return a list(convert the set to list implicitly)
        wordOfTagCollection[x] = set(wordOfTagCollection[x])
        for word in wordOfTagCollection[x]:
            wordString += word
            wordString += ' '
        #print(wordOfTagCollection[x])
        #print(len(wordOfTagCollection[x]))
        wordString += '\n'
        fileW.write(wordString)
        wordString = ''

    # after being able to read from file wordOfTag.txt and convert it to wordOfTagCollection, the
    # following code will be move to a separate function, so that wordOfTag no longer need to be
    # generated every time for testing/prediction
    fileRTestDataFIC = open(testDataPathForFIC, "r")
    testText = fileRTestDataFIC.read().splitlines()
    #print(len(testText))

    # to apply spread activation, here we first get the matrix of weight ready
    fileRMatrixOfWeight = open(matrixOfWeight, "r")
    weightMatrix = fileRMatrixOfWeight.read().splitlines()
    weight3Dlist = []
    for line in weightMatrix:
        weight2Dlist = []
        indexOfTag = 0
        for weight in line.split(" "):
            weight1Dlist = []
            if weight!='0' and weight!='':
                weight1Dlist.append(weight)
                weight1Dlist.append(indexOfTag)
                weight2Dlist.append(weight1Dlist)
            #elif weight=='0':
            #    weight1Dlist.append(-1)
            #    weight1Dlist.append(-1)
            #    weight2Dlist.append(weight1Dlist)
            indexOfTag+=1
        weight3Dlist.append(weight2Dlist)
    #print(len(weight3Dlist))
    printIndex = 0
    for tagAsso in weight3Dlist:
        #print(printIndex)
        printIndex+=1
        #print(tagAsso)
    #print(weight3Dlist)
    i = 0
    j = 0
    fileWFICtopK = open(FICtopKPath, "w")
    fileWFICtopKvalue = open(FICtopKValuePath, "w")
    tagHitMap = {}
    tagHitMapIndexOfTagVersion = {}
    print("FIC is predicting the Top " + str(top_n) + " tags for the test data:", flush=True) # SA()
    predictedTagString = ""
    predictedTagValue = ""
    currentRow = oneTenth
    for line in testText:
        nounArray = []
        j+=1
        #if (j == currentRow):
        #    print(i*10, "%", end="|", flush=True)
        #    currentRow += oneTenth
        #    i+=1
        if j%500 == 0:
            print(j, end="|", flush=True)
        #if j == 1:
        #    break
        splitedArray = line.split(' ')
        for element in splitedArray:
            if element != '':
                nounArray.append(element)
        #print(nounArray)
        totalNumOfNounInAPost = len(nounArray)
        if totalNumOfNounInAPost == 0:
            print(j)
            print("Here is a row contains no nouns, please delete it and re-generate testing data")
        #print(totalNumOfNounInAPost)
        # only show 10 results out of 438 (in 5k table) test posts. Get rid of i to show all
        #if i < 5:
        #tempHitToString = ''
        for x in range(n):
            hit = 0
            for noun in nounArray:
                if noun in wordOfTagCollection[x]:
                    hit += 1
                #hit += wordOfTagCollection[x].count(noun)
            tagHitMapIndexOfTagVersion[x] = hit
            #tagHitMap[tagSet[x]] = hit
            tagHitMap[x] = hit # item 0(key): tag index /// item 1(value): hit
        sorted_map = sorted(tagHitMap.items(), key=operator.itemgetter(1))
        sorted_map_top_n = sorted_map[-top_n:]
        top_n_map = {}
        for tagHitPair in sorted_map_top_n:
            top_n_map[tagHitPair[0]] = tagHitPair[1]
        #print(top_n_map)
        new_tag_map = {}
        for key, value in top_n_map.items():
            #print(key,value)
            if weight3Dlist[key]:
                for weightTagPair in weight3Dlist[key]:
                    newTagIndex = weightTagPair[1]
                    #print("new tag: " + str(newTagIndex))
                    newTagWeight = float(weightTagPair[0])*value*1 # int here is the tentative way to boost the weight
                    #print("weight: "+ str(newTagWeight))
                    #print("is it in the map????????????????????????????????????????")
                    if newTagIndex in top_n_map:
                        top_n_map[newTagIndex] = top_n_map[newTagIndex] + newTagWeight
                        #print("Yes! updating the value of the tag")
                    else:
                        if newTagIndex in new_tag_map:
                            new_tag_map[newTagIndex] = new_tag_map[newTagIndex] + newTagWeight
                            #print("No! But existed in new tag map, updating the value of the tag")
                        else:
                            new_tag_map[newTagIndex] = newTagWeight
                            #print("No! Also not exisetd in new tag map, adding to the new tag map")
                        #new_tag_map[weightTagPair[1]] = float(weightTagPair[0])*value
            #if not weight3Dlist[key]:
            #    print("None Associated Tag Found for tag " + str(key))
        #print("print new tag map")
        #print(len(new_tag_map))
        #print(len(top_n_map))
        top_M_new_map = top_n_map.copy()
        top_M_new_map.update(new_tag_map)
        #print("updated top M tags from FIC contains: " + str(len(top_M_new_map)) + " tags")
        sorted_top_M_new_map = sorted(top_M_new_map.items(), key=operator.itemgetter(1))
        sorted_top_M_new_map_top_n = sorted_top_M_new_map[-top_n:]

        # uncomment this block to show if there is any new added tags after applying Spreading Activation
        # <block start>
        #newTagList = []
        #for tagHitPair in sorted_top_M_new_map_top_n:
        #    newTagList.append(tagHitPair[0])
        #newTagSet = set(newTagList)
        #formalTagList = []
        #for tagHitPair in sorted_map_top_n:
        #    formalTagList.append(tagHitPair[0])
        #formalTagSet = set(formalTagList)
        ##print(len(newTagList))
        ##print(len(formalTagList))
        #numOfNewAddedTag = len(newTagSet.difference(formalTagSet))
        #print("# of new tag added: " + str(numOfNewAddedTag))
        # <block end>

        #print(totalNumOfNounInAPost)
        sorted_top_M_new_map_top_n= [(pair[0], pair[1]/totalNumOfNounInAPost) for pair in sorted_top_M_new_map_top_n]
        #print(sorted_top_M_new_map_top_n)
        printIndex = 0
        for predictedTag in sorted_top_M_new_map_top_n:
            #predictedTagString += str(predictedTag[0]) # store tag index
            predictedTagString += tagSet[predictedTag[0]] # store tag string
            predictedTagString += "|"
            predictedTagValue += str(predictedTag[1])
            predictedTagValue += "|"

        predictedTagString += '\n'
        predictedTagValue += '\n'
        #i += 1
    #print(predictionOfFIC)
    fileWFICtopK.write(predictedTagString)
    fileWFICtopKvalue.write(predictedTagValue)

    fileRTestDataFIC.close()
    fileWFICtopK.close()
    fileWFICtopKvalue.close()
    fileRMatrixOfWeight.close()
    fileW.close()
    fileR.close()

def sA(runFlag): # first method of spreading activation
    print("\nCalling sA()")
    fileW = open(matrixOfWeight, "w")
    fileR = open(keyTagFilePath, "r")
    tagSet = fileR.read().splitlines()
    n = len(tagSet)

    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    #cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    #totalrow = int(cursor.fetchone()[0])
    #oneTenth = math.floor(totalrow / 10)
    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    print(totalrow)

    query = "SELECT RowNum, KeyTags FROM " + table# + " LIMIT 100" #+ str(oneTenth) + ", 999999999"
    cursor.execute(query)
    #oneTenth = math.floor(oneTenth*0.9) # 10 fold cross validation

    #count = 1
    #i = 1
    #currentRow = oneTenth
    setofTags = []
    for (RowNum, KeyTags) in cursor:
        try:
            tagSetOfOnePost = []
            for tag in re.split(r">|<", KeyTags):
                #print(tag)
                if tag != '"' and tag != '':
                    index = tagSet.index(tag)
                    tagSetOfOnePost.append(index)
            setofTags.append(tagSetOfOnePost)
        except Exception:
            print(sys.exc_info()[0])
            print(traceback.format_exc())
    #print(setofTags)

    #totalrow = 100 # delete after testing code !!!!!!!!!!!!!!!!!
    # initialize a two dimension array
    twoDArray_tag_set = []
    for i in range(0,topN):
        temp = []
        for j in range(0, totalrow):
            temp.append(0)
        twoDArray_tag_set.append(temp)
    #print(twoDArray_tag_set)

    i = 0
    for set in setofTags:
        for tagIndex in set:
            twoDArray_tag_set[tagIndex][i] = 1
        i += 1
    #print(twoDArray_tag_set)

    #topn = 50 # delete this after testing code (should use topN the global value) !!!!!!!!!!!!
    topn = topN
    twoDArray_tag_tag = []
    #for i in range(0,topN):
    #    temp = []
    #    for j in range(0, topN):
    #        temp.append(0)
    #    twoDArray_tag_tag.append(temp)
    #print(twoDArray_tag_tag)
    for i in range(0,topn):
        temp = []
        for j in range(0, topn):
            temp.append(0)
        twoDArray_tag_tag.append(temp)
    #print(twoDArray_tag_tag)

    if runFlag == True:
        #for i in range(0, topN-1):
        #    for k in range(1, topN):
        associatedTagsIndex = []
        for i in range(0, topn):
            #linkedCount = ''
            tempIndexList = []
            for k in range(0, topn):
                count = 0
                for j in range(0, totalrow):
                    if twoDArray_tag_set[i][j] == 1 and twoDArray_tag_set[k][j] == 1:
                        count += 1
                weight = count/(tagCount[i]+tagCount[k]) #tagCount has to be significant enough
                if weight > 0.1 and weight != 0.5:
                    tempIndexList.append(k)
                    twoDArray_tag_tag[i][k] = weight # Threshold 0.1 (can increase as data size increase)
                else:
                    twoDArray_tag_tag[i][k] = 0
            associatedTagsIndex.append(tempIndexList)
                #print(tagCount[i])
                #print(tagCount[k])
                #linkedCount += str(count)
                #linkedCount += ' '
            #print(linkedCount)
        #print(twoDArray_tag_tag)
        #print(tagCount)
        print(len(associatedTagsIndex))

    for array in twoDArray_tag_tag:
        weight = ''
        for element in array:
            weight += str(element)
            weight += ' '
        fileW.write(weight + '\n')
        #print(weight)
    #associatedTags = []
    #i = 0
    #for array in twoDArray_tag_tag:
    #    #print(i)
    #    for tagIndex in associatedTagsIndex[i]:
    #        print(array[tagIndex])
    #    if i < (topn -2):
    #        i += 1
    #    #for element in array:
    #    #    if element != 0:
    #    #        print(array.index)
    #    #        element = 0
    #    #print(array)
    #    #sortedArray = sorted(array, reverse=True)
    #    #print(sortedArray[:5])


    fileR.close()
    fileW.close()


def processOutputOfBIC():
    print("\nCalling processOutputOfBIC")
    fileR = open(topTagsPath, "r")
    topTagCollection = set(fileR.read().splitlines())
    #print(topTagCollection)
    tagCountMap= {}
    tagCountMap[0] = 0
    tagCountMap[1] = 0
    tagCountMap[2] = 0
    tagCountMap[3] = 0
    tagCountMap[4] = 0
    tagCountMap[5] = 0

    fileR1 = open(outputOfBICPath, "r")
    fileR2 = open(keyTagFilePath, "r")
    fileW = open(BICtopKPath, "w")
    fileW1 = open(BICtopKvaluePath, "w")
    tagSet = fileR2.read().splitlines()
    i=0
    predictedTagString = ""
    predictedTagValueString = ""
    for line in fileR1:
        list = []
        map = {}
        #if i<5:
        #print(line.split(" "))
        for pair in line.split((" ")):
            temp = []
            if pair != '\n':
                for item in pair.split(":"):
                    temp.append(item)
                list.append(temp)
        for pair in list:
            #print(int(pair[0]))
            #print(float(pair[1]))
            map[tagSet[int(pair[0])]] = float(pair[1])
        sorted_map = sorted(map.items(), key=operator.itemgetter(1))
        sorted_map_top_n = sorted_map[-top_n:]
        #maxValue = sorted_map_top_n[-1][1]
        #maxValue = 1
        #print(maxValue)
        #sorted_map_top_n = [(pair[0], pair[1]) for pair in sorted_map_top_n]

        #print(map['xml'])
        #print(sorted_map['xml'])  # sorted map is no longer a map, thus this line gives error

        #print(sorted_map[-10:])
        #predictionBIC.append(sorted_map[-10:])
        predictionOfBIC.append(sorted_map_top_n)
        tempCount = 0
        for predictedTag in sorted_map_top_n:
            tagTemp = predictedTag[0]
            if tagTemp not in topTagCollection:
                tempCount += 1
                predictedTagString += tagTemp
                predictedTagString += "|"
                predictedTagValueString += str(predictedTag[1])
                predictedTagValueString += "|"
        predictedTagString += '\n'
        predictedTagValueString += '\n'
        tagCountMap[tempCount] += 1
        i+=1
    print("Outputing Top " + str(top_n) + " tag predictions from BIC:")
    for key, value in tagCountMap.items():
        tagCountMap[key] = value/i*100
    print(tagCountMap)
    fileW.write(predictedTagString)
    fileW1.write(predictedTagValueString)
    #print(predictionOfBIC)
        #print(list)
    fileR1.close()
    fileR2.close()
    fileW.close()
    fileW1.close()
    fileR.close()

def processOutputOfBICForRestTags():
    print("\nCalling processOutputOfBICForRestTags")

    fileR1 = open(outputOfBICPathForRestTags, "r")
    fileR2 = open(restTagFilePath, "r")
    fileW = open(BICtopKPathForRestTags, "w")
    fileW1 = open(BICtopKValuePathForRestTags, "w")
    tagSet = fileR2.read().splitlines()
    i=0
    predictedTagString = ""
    predictedTagValueString = ""
    for line in fileR1:
        list = []
        map = {}
        #if i<5:
        #print(line.split(" "))
        for pair in line.split((" ")):
            temp = []
            if pair != '\n':
                for item in pair.split(":"):
                    temp.append(item)
                list.append(temp)
        for pair in list:
            #print(int(pair[0]))
            #print(float(pair[1]))
            map[tagSet[int(pair[0])]] = float(pair[1])
        sorted_map = sorted(map.items(), key=operator.itemgetter(1))
        sorted_map_top_n = sorted_map[-top_n:]
        #maxValue = sorted_map_top_n[-1][1]
        #maxValue = 1
        #print(maxValue)
        #sorted_map_top_n = [(pair[0], pair[1]) for pair in sorted_map_top_n]

        #print(map['xml'])
        #print(sorted_map['xml'])  # sorted map is no longer a map, thus this line gives error

        #print(sorted_map[-10:])
        #predictionBIC.append(sorted_map[-10:])
        predictionOfBIC.append(sorted_map_top_n)
        tempCount = 0
        for predictedTag in sorted_map_top_n:
            tagTemp = predictedTag[0]
            predictedTagString += tagTemp
            predictedTagString += "|"
            predictedTagValueString += str(predictedTag[1])
            predictedTagValueString += "|"
        predictedTagString += '\n'
        predictedTagValueString += '\n'
        i+=1
    print("Outputing Top " + str(top_n) + " tag predictions from BIC:")
    fileW.write(predictedTagString)
    fileW1.write(predictedTagValueString)
    #print(predictionOfBIC)
        #print(list)
    fileR1.close()
    fileR2.close()
    fileW.close()
    fileW1.close()

def return2ndGiven1st(list, value1st):
    value2nd = 0
    found = False
    for pair in list:
        if pair[0] == value1st:
            value2nd = pair[1]
            found = True
            break
    if not found:
        value2nd = 0
    return float(value2nd)

def averageRecall(list3D, a, b, k, testing):
    if a == 0 and b == 0:
        return 0
    else:
        # by default, computing recall for tuning
        comparisonTagList = tagsListOfTuningdata
        # when arg = 1, computing recall for testing
        if testing == 1:
            comparisonTagList = tagsListOfTestingData
        recall = 0
        i = 0
        for list2D in list3D:
            tempMap = {}
            finalPredictedTags = []
            for list1D in list2D:
                finalScore = a*list1D[1] + b*list1D[2]
                tempMap[list1D[0]] = finalScore
            sorted_map = sorted(tempMap.items(), key=operator.itemgetter(1))
            #print(sorted_map)
            sorted_map_top_k = sorted_map[-k:]
            #print(sorted_map_top_k)
            for pair in sorted_map_top_k:
                finalPredictedTags.append(pair[0])
            #if i == 0 and testing == 1:
            #    print("final predicted")
            #    print(finalPredictedTags)
            #    print("actual")
            #    print(comparisonTagList[i])
            tagsIntersect = list(set(comparisonTagList[i]) & set(finalPredictedTags))
            #print(tagsIntersect)
            #print(len(tagsIntersect))
            #print(len(tagsListOfTuningdata[i]))
            recall += len(tagsIntersect) / len(comparisonTagList[i])
            #if i < 100 and testing == 1:
            #    print(len(tagsIntersect))
            #    print(len(comparisonTagList[i]))
            #    print("---------------------")
            if i == 1000 and testing == 1:
                print("recall sum: " + str(recall))
            #print(recall)
            i+=1
        #print("average recall")
        #print(len(list3D))
        recall = recall / len(list3D)
        #print(recall)
        return recall

def tuningAlphaBeta(): # tuning alpha beta for composer
    alpha = 0
    beta = 0
    fileR = open(testDataKeyTagsPathForTuning, "r")
    for tags in fileR.read().splitlines():
        tagSetOfOnePost = []
        for tag in re.split(r">|<", tags):
            #print(tag)
            if tag != '"' and tag != '':
                #index = tagSet.index(tag)
                #tagSetOfOnePost.append(index)
                tagSetOfOnePost.append(tag)
        tagsListOfTuningdata.append(tagSetOfOnePost)
    #print("tagsListtttttttttttttttttttttttttt")
    #print(tagsListOfTuningdata)
    fileR.close()
    print("\nCalling tuningAlphaBeta")
    #remember to remove i restriction on processOutputOfBIC and wordListForTag
    tuningSetSize = len(tagsListOfTuningdata)
    print("Tuning size in tuningAlphaBeta:")
    print(tuningSetSize)
    tagUnionOfTuningSet = []
    for i in range(0, tuningSetSize):
        #print(predictionOfBIC[i])
        #print(predictionOfFIC[i])
        tempTagUnion = []
        for pair in predictionOfBIC[i]:
            tempTagUnion.append(pair[0])
        for pair in predictionOfFIC[i]:
            tempTagUnion.append(pair[0])
        tempTagUnion= list(set(tempTagUnion))
        tagUnionOfTuningSet.append(tempTagUnion)
    #print(tagUnionOfTuningSet)
    tagUnionOfTuningSetWithScores = []

    list3D = []
    for i in range(0, tuningSetSize):
        list2D = []
        #print("-----------------------------------")
        #j = 0
        for tag in tagUnionOfTuningSet[i]:
            list1D = []
            list1D.append(tag)
            #list1D.append(j)
            list1D.append((return2ndGiven1st(predictionOfBIC[i], tag)))
            list1D.append((return2ndGiven1st(predictionOfFIC[i], tag)))
            list2D.append(list1D)
        #    j+=1
        list3D.append(list2D)
    #print(list3D)

    a = 0 # alpha
    b = 0 # beta
    gridSearchMap = {}
    for i in range(0, 11): # not 2, should be 11
        a = i/10
        for j in range(0, 11): # should be 11
            b = j/10
            key = str(a) + "|" + str(b)
            #print("---------------------------------------------")
            gridSearchMap[key] = averageRecall(list3D,a,b,RECALLatK,0) # 0 for tuning
            #aRecall = averageRecall(list3D,a,b,k)
            #print(str(aRecall) + " -- a: " + str(a) + " b: " + str(b))
    sorted_gridSearchMap = sorted(gridSearchMap.items(), key=operator.itemgetter(1))
    #print(sorted_gridSearchMap[-1][0])
    #print(sorted_gridSearchMap[-10:])
    pair = []
    alpha = sorted_gridSearchMap[-1][0].split("|")[0]
    beta = sorted_gridSearchMap[-1][0].split("|")[1]
    pair.append(alpha)
    pair.append(beta)
    return pair

def composer():
    fileWFianlTopK = open(FinaltopKPath, "w")
    fileRFICtopK = open(FICtopKPath, "r")
    fileRFICtopKvalue = open(FICtopKValuePath, "r")
    fileR = open(testDataKeyTagsPathForTuning, "r")
    fileRTestDataKeys = open(testDataKeyTagsPath, "r")

    for tags in fileRTestDataKeys.read().splitlines():
        tagSetOfOnePost = []
        for tag in re.split(r">|<", tags):
            #print(tag)
            if tag != '"' and tag != '':
                #index = tagSet.index(tag)
                #tagSetOfOnePost.append(index)
                tagSetOfOnePost.append(tag)
        tagsListOfTestingData.append(tagSetOfOnePost)
    #print(len(tagsListOfTestingData))

    predictionOfFICtagPart = []
    for tags in fileRFICtopK.read().splitlines():
        tempList = []
        #if i<2:
        for tag in tags.split("|"):
            if tag != '':
                #print(tag)
                tempList.append(tag)
        predictionOfFICtagPart.append(tempList)
        #i+=1
    #print(len(predictionOfFICtagPart))
    i = 0
    for values in fileRFICtopKvalue.read().splitlines():
        list2D = []
        j = 0
        for value in values.split("|"):
            list1D = []
            if value != '':
                list1D.append(predictionOfFICtagPart[i][j])
                list1D.append(value)
                list2D.append(list1D)
                j+=1
        predictionOfFIC.append(list2D)
        i+=1
    #print(len(predictionOfFIC))

    pair = tuningAlphaBeta()
    alpha = float(pair[0])
    beta = float(pair[1])
    alpha = 15
    beta = 1
    print("alpha: " + str(alpha))
    print("beta: " + str(beta))
    if len(predictionOfFIC) != len(predictionOfBIC):
        print("ERROR! These two lists have different length!")
        print(len(predictionOfBIC))
        print(len(predictionOfFIC))
    total = len(predictionOfFIC)
    tagUnionOfBICAndFIC = []
    for i in range(0, total):
            #print(predictionOfBIC[i])
            #print(predictionOfFIC[i])
            tempTagUnion = []
            for pair in predictionOfBIC[i]:
                tempTagUnion.append(pair[0])
            for pair in predictionOfFIC[i]:
                tempTagUnion.append(pair[0])
            tempTagUnion= list(set(tempTagUnion))
            tagUnionOfBICAndFIC.append(tempTagUnion)
    list3D = []
    for i in range(0, total):
        list2D = []
        #print("-----------------------------------")
        #j = 0
        for tag in tagUnionOfBICAndFIC[i]:
            list1D = []
            list1D.append(tag)
            #list1D.append(j)
            list1D.append(return2ndGiven1st(predictionOfBIC[i], tag))
            list1D.append(return2ndGiven1st(predictionOfFIC[i], tag))
            list2D.append(list1D)
            #j+=1
        list3D.append(list2D)
    #print(len(list3D))
    #print(list3D)
    finalOutputString = ""
    finalPredictionOfOnePost = []
    for list2D in list3D:
        tempMap = {}
        for list1D in list2D:
            tempMap[list1D[0]] = alpha*list1D[1] + beta*list1D[2]
        sorted_map = sorted(tempMap.items(), key=operator.itemgetter(1))
        sorted_map_top_k = sorted_map[-RECALLatK:]
        #print(sorted_map_top_k)
        for pair in sorted_map_top_k:
            finalOutputString += pair[0]
            finalOutputString += "|"
            finalPredictionOfOnePost.append(pair[0])
        finalOutputString += "\n"
    print("Recall@" + str(RECALLatK) + " of EnTagRec")
    print(averageRecall(list3D,alpha,beta,RECALLatK,1)) # 1 for testing

    fileWFianlTopK.write(finalOutputString)

    fileRFICtopK.close()
    fileWFianlTopK.close()
    fileRFICtopKvalue.close()
    fileRTestDataKeys.close()


def readTest():
    #fileRBIC = open(trainingDataPathBIC, "w")
    fileRFIC = open(trainingDataPathFIC, "r")
    i = 0
    for line in fileRFIC.read().splitlines():
        if i > 3:
            break
        else:
            print(line)
        i+=1
    fileRFIC.close()

def recallOfFICorBIC(mode):
    if mode == 1:
        fileRPredicted = open(FICtopKPath, "r")
        print("\nRecall@" + str(RECALLatK) + " of FIC is: ")
    elif mode == 2:
        fileRPredicted = open(BICtopKPath, "r")
        print("\nRecall@" + str(RECALLatK) + " of BIC is: ")
    elif mode == 3:
        fileRPredicted = open(BICtopKPathForRestTags, "r")
        print("\nRecall@" + str(RECALLatK) + " of BIC (For Rest Tags) is: ")
    fileRActual = open(testDataKeyTagsPathForRestTags, "r")

    predictedTagsCollection = []
    actualTagsCollection = []
    for line in fileRPredicted.read().splitlines():
        tagOfOnePost = []
        for tag in line.split("|"):
            if tag != '':
                tagOfOnePost.append(tag)
        predictedTagsCollection.append(tagOfOnePost)
    #print(predictedTagsCollection)

    i = 0
    for line in fileRActual.read().splitlines():
        tagOfOnePost = []
        for tag in re.split(r">|<", line):
            if tag != '""' and tag != '"' and tag != '':
                tagOfOnePost.append(tag)
        actualTagsCollection.append(tagOfOnePost)
    #print(actualTagsCollection)
    recall = 0
    total = len(predictedTagsCollection)
    count = 0
    #print(total)
    for i in range(0, total):
        #try:
        if len(actualTagsCollection[i]) != 0:
            recall += len(list(set(predictedTagsCollection[i]) & set(actualTagsCollection[i])))/len(actualTagsCollection[i])
            count += 1
        #except Exception:
        #    print(total)
        #    print(i)
    recall = recall/count
    #recall = recall/total
    print(recall)
    fileRPredicted.close()
    fileRActual.close()

def bodyProcessorAlt():
    print("Calling bodyProcessor (Alt) ():")

    cnx = mysql.connector.connect(**config)
    #cnx = mysql.connector.connect(user=username, password=pw, database=db)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    oneTenth = math.floor(totalrow / 10)

    query = "SELECT RowNum, Body FROM " + table
    #query = ("SELECT RowNum, Body FROM stack50kview30k")

    cursor.execute(query)
    cCSBody = [] # camel case split & number removed
    tokenizedBody = []
    stopWordsRemoved = []
    stemming = []
    processedText = ''
    ps = PorterStemmer()
    updateList = []
    updateQuery = ''

    stop_words = set(stopwords.words("english"))
    # for (RowNum, Tag, Body, ProcessedBody, ViewCount, Score) in cursor:
    print("Processing Body (Alt): ")
    count = 0
    i = 1
    currentRow = oneTenth
    for (RowNum, Body) in cursor:
        count += 1
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        if count == 2:
            break
        #soup = BeautifulSoup(Body, 'xml') # can use it because lxml only avaliable for python 3.2, when pip install lxml
        # on python 3.4, you get errors.
        soup = BeautifulSoup(Body, 'html.parser')
        updateQuery = "UPDATE " + table + " SET ProcessedBodyAlt=\"\"\"" + soup.get_text().lower() + "\"\"\" WHERE RowNum=" + str(RowNum) # id is int, so might need to take out ' '
        print(updateQuery)
        updateList.append(updateQuery)
        # clean all containers
        expandContractions = ''
        tokenizedBody = []
        cCSBody = []
        stopWordsRemoved = []
        stemming = []
        processedText = ''


    # cursor.execute("""UPDATE idbody10 SET ProcessedBody='haha1' WHERE RowNum='104'""")
    print("\nUpdating table")
    count = 0
    i = 1
    currentRow = oneTenth
    for query in updateList:
        #print(query)
        cursor.execute(query)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
    cnx.commit()

    cursor.close()
    cnx.close()

def keepOnlyNounAlt():
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()

    cursor.execute("SELECT COUNT(RowNum) FROM " + table)
    totalrow = int(cursor.fetchone()[0])
    totalrow = 800
    oneTenth = math.floor(totalrow / 10)
    oneHundredth = math.floor(totalrow / 100)

    query = "SELECT RowNum, ProcessedBodyAlt FROM " + table + " LIMIT 201, 1000" #+ " WHERE RowNum = 23"
    #query = "SELECT RowNum, Body FROM " + table #+ " LIMIT 1" #+ " WHERE RowNum = 23"
    cursor.execute(query)
    list_nouns = []
    only_nouns = ''
    updateList = []
    count = 0
    i = 1
    currentRow = oneHundredth
    print('\nPOS tagging and filtering...(this might take a long time)')
    wordPattern = re.compile("^[a-z]+$")
    for (RowNum, ProcessedBodyAlt) in cursor:
    #for (RowNum, Body) in cursor:
        #if count == 10: # number has to be equal to LIMIT # above (or greater)
        #    break
        updateQuery = ''
        stanfordPOStagger = StanfordPOSTagger("C:\\Python34\\Lib\\site-packages\\nltk\\tag\\models\\english-left3words-distsim.tagger","C:\\Python34\\Lib\\site-packages\\nltk\\tag\\stanford-postagger-3.5.2.jar")
        for taggedWord in stanfordPOStagger.tag(ProcessedBodyAlt[1:].split()):
            if taggedWord[1] == 'NN' or taggedWord[1] == 'NNP' or taggedWord[1] == 'NNS' or taggedWord[1] == 'NNPS':
                list_nouns.append((taggedWord[0]))
                #print(taggedWord[0])
        for w in set(list_nouns):
            only_nouns += w
            only_nouns += ' '
        updateQuery = "UPDATE " + table + " SET OnlyNounBodyAlt=\"\"\"" + only_nouns + "\"\"\" WHERE RowNum=" + str(RowNum)
        #print(updateQuery)
        updateList.append(updateQuery)

        list_nouns = []
        only_nouns = ''

        if (count == currentRow):
            print(i, "%", end="|", flush=True)
            currentRow += oneHundredth
            i+=1
        count += 1

    #print(list_nouns)
    #print(stemmed_nouns)

    print("\nUpdating the OnlyNounBodyAlt column")
    count = 0
    i = 1
    currentRow = oneTenth
    for query in updateList:
        #print(query)
        cursor.execute(query)
        if (count == currentRow):
            print(i*10, "%", end="|", flush=True)
            currentRow += oneTenth
            i+=1
        count += 1
    cnx.commit()

    cursor.close()
    cnx.close()

#MAIN
# step 1
#bodyProcessor()
#keepOnlyNoun() # might take an hour !!
#bodyProcessorAlt()
#keepOnlyNounAlt()
#filterBadNouns(GLOBAL[3]) # filter out nouns that appear less than N times
                # UNLESS you don't use the DELETE line in this method. You don't have to use the line
                # use the DELETE line can get you 0.5-1.0% more for recall@k. Not using it can let you try different threshold like 5,10,15,20,...
                # if you decide to use the DELETE line, since it might delete some rows, so this has to be run before pretty much every thing expect the first two functions (because it can change the amount of consistent tag thus everything thing)
# step 2
#keyTagListGen() # RUN this with sA()  // always run sA() again when this ran
#keyTagsWriteToTable()
#removeEmptyKeyTagRow()
#sA(False) # run with keyTagListGen // takes about 20 min for 50k posts to generate the matrix of weight table
           # False: fake matrixOfWeight table to save time. True: Real table
#trainingDataGen(1, 1)
###readTest()
#testDataGenForBICandFIC()
#tuningDataSetGen()
# step 3
# run L-LDA in java (eclipse) then the rest of the program // rerun this when ekyTagListGen ran
# step 4
#wordListForTag() # if top_N (global) changed, this has to be run again to get recall@k for EnTagRec
#processOutputOfBIC()
#composer() # have to run processOutputOfBIC to get global predictionOfFIC and predictionOfBIC in order to tune alpha and beta

# Evaluation
#recallOfFICorBIC(1) # 1 for FIC, 0 for BIC
#recallOfFICorBIC(0)


# other:
# run L-LDA in java (eclipse) then the rest of the program // rerun this when keyTagListGen ran
if GLOBAL[4] == 'E':
    tuningDataSetGen()
    wordListForTag()
    processOutputOfBIC()
    composer()
elif GLOBAL[4] == 'F':
    wordListForTag()
    recallOfFICorBIC(1)
elif GLOBAL[4] == 'B':
    processOutputOfBIC()
    recallOfFICorBIC(0)
elif GLOBAL[4] == 'T':
    processOutputOfBIC()
    #userTagsFilterOutTopTags(True, False) # if need to updateTable, set first arg to true.
    #checkIfInCategories()
elif GLOBAL[4] == "Rest":
    #userTagsFilterOutTopTags(True, True)
    #trainingDataGenForRestTags(1, 0)
    #testDataGenForBICandFICForRestTags()
    #processOutputOfBICForRestTags()
    recallOfFICorBIC(3)
else:
    # only need to run once
    #bodyProcessor()
    #keepOnlyNoun() # might take an hour !!
    keyTagListGen() # RUN this with sA()  // always run sA() again when this ran

    ###keyTagsWriteToTable()
    ###removeEmptyKeyTagRow()

    #sA(False)
    ##sA(True)
    ##stemNounsForFIC()

    ###filterBadNouns(GLOBAL[3])
    ###trainingDataGen(1, 1)
    ###testDataGenForBICandFIC()
    print("preprocessor done")


print('DONE!')

