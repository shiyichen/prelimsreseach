__author__ = 'Shiyi'
import mysql.connector
from mysql.connector import errorcode

TABLENAME = 'stack50kview30k_3'
FILENAME = '50kview30k.csv'
def createTables(cnx):
    TABLES = {}
    TABLES[TABLENAME] = (
        #"CREATE TABLE `stack50k2008` ("
        "CREATE TABLE " + TABLENAME + " ("
        "  `RowNum` int(10) NOT NULL AUTO_INCREMENT,"
        "  `Id` int(20),"
        "  `Tags` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci,"
        "  `KeyTags` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci,"
        "  `Body` text CHARACTER SET utf8 COLLATE utf8_general_ci,"
        "  `ProcessedBody` text CHARACTER SET utf8 COLLATE utf8_general_ci,"
        "  `OnlyNounBody` text CHARACTER SET utf8 COLLATE utf8_general_ci,"
        "  `GoodNouns` text CHARACTER SET utf8 COLLATE utf8_general_ci,"
        "  `ViewCount` int(20),"
        "  `Score` int(10),"
        "  PRIMARY KEY (`RowNum`)"
        ") ENGINE=InnoDB")

    for name, ddl in TABLES.items():
        try:
            print("Creating table {}: ".format(name), end='')
            cursor.execute(ddl)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                print("already exists.")
            else:
                print(err.msg)
        else:
            print("OK")

def loadData(cnx):
    try:
        #loadDataQuery = """LOAD DATA LOCAL INFILE 'C:\\\\wamp\\\\sqlData\\\\50k2008.csv' INTO TABLE stack50k2008 FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\\n' (@col1, @col2, @col3, @col4, @col5) set Id=@col1, Tags=@col2, Body=@col3, ViewCount=@col4, Score=@col5"""
        loadDataQuery = """LOAD DATA LOCAL INFILE 'C:\\\\wamp\\\\sqlData\\\\""" +FILENAME + """' INTO TABLE """ + TABLENAME + """ FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\\n' (@col1, @col2, @col3, @col4, @col5) set Id=@col1, Tags=@col2, Body=@col3, ViewCount=@col4, Score=@col5"""
        #print(loadDataQuery)
        cursor.execute(loadDataQuery)
        cnx.commit()
    except:
        print("File Loading Failed")
    else:
        print("File Loading Succeed")

def ridSomeRows(cnx):
    #Delete problematic rows:
    try:
        #query = """DELETE FROM stack50k2008 WHERE Id = 0 OR Tags IS NULL OR Body IS NULL OR Body IS NULL"""
        query = """DELETE FROM """ + TABLENAME + " WHERE Id = 0 OR Tags IS NULL OR Body IS NULL OR Body IS NULL"""
        cursor.execute(query)
        cnx.commit()
    except:
        print("RidSomeRows Failed")
    else:
        print("RidSomeRows Succeed")


#main
config = {
    'user': 'root',
    'password': 'jTGxXEdxwUdppbfp',
    'host': 'localhost',
    'database': 'stackoverflow',
}

try:
    cnx = mysql.connector.connect(**config)
    cnx.set_charset_collation('utf8', 'utf8_general_ci')
    cursor = cnx.cursor()
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)

#before all this, it's better to delete the first row of the csv table.
createTables(cnx)
loadData(cnx)
ridSomeRows(cnx)

cursor.close()
cnx.close()
