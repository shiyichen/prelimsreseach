from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

example_sentence = "This is an example showing off stop word filtration."
stop_words = set(stopwords.words("english"))
print(stopwords.words("english"))
print(stop_words)
words = word_tokenize(example_sentence)
count = 0
for x in range(0,1000):
    count += 1
    if (count % 50 == 0):
        print(count)
# filtered_sentenec = []
# for w in words:
#     if w not in stop_words:
#         filtered_sentenec.append(w)
#
# print(filtered_sentenec)

# filtered_sentence = [w for w in words if not w in stop_words]
#
# print(filtered_sentence)

