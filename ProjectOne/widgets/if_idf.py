__author__ = 'Shiyi'
from textblob import TextBlob as tb
import math, json

directory = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\stack50kview30k_3_all_tags_3\\"
keyTagFilePath = directory + "all_tags.txt"
wordOfTagPath = directory + "wordOfTag.txt"
word_freq_map_per_tag_path = directory + "word_freq_map_per_tag.json"
all_word_collection_path = directory + "all_word_collection.json"
#wordToVectorPath = "C:\\Users\\Shiyi\\Google Drive\\research\\NLPA\\prelimsreseach\\LLDA\\iofiles\\stack50kview30k_3_all_tags\\wordToVector.txt"
#word

def tf(word, blob):
    return blob.words.count(word) / len(blob.words)

def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob)

def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)

fileR1 = open(wordOfTagPath, "r", encoding="utf-8")
fileR2 = open(keyTagFilePath, "r")
fileW = open(word_freq_map_per_tag_path, "w")
fileW2 = open(all_word_collection_path, "w")

tagList = fileR2.read().splitlines()
bloblist = []
for list in fileR1.read().splitlines():
    bloblist.append(tb(list))

word_freq_map_per_tag = {}
all_word_collection_set = set()
all_word_collection = []
for i, blob in enumerate(bloblist):
    print("Top words in document {}".format(i + 1))
    scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
    sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
    tempMap = {}
    tempLength = 0
    if len(sorted_words) > 1000:
        tempLength = 1000
    else:
        tempLength = len(sorted_words) - 1
    for word, score in sorted_words[:tempLength]:
        tempMap[word] = score
        all_word_collection_set.add(word)
        #print("\tWord: {}, TF-IDF: {}".format(word, round(score, 10)))
    word_freq_map_per_tag[tagList[i]] = tempMap

for word in all_word_collection_set:
    all_word_collection.append(word)
#print(all_word_collection)
#print(word_freq_map_per_tag)
fileW.write(json.dumps(word_freq_map_per_tag))
fileW2.write(json.dumps(all_word_collection))

fileR1.close()
fileR2.close()
fileW.close()
fileW2.close()


