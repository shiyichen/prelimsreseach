from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

ps = PorterStemmer()
example_words = ["python", "pythoner", "pythoning", "pythoned", "pythonly"]
# for w in example_words:
#     print(ps.stem(w))
new_text = "this is a sample Database with an integrated test suite, used to test your applications and database servers"
words = word_tokenize(new_text)
for w in words:
    print(ps.stem(w))

