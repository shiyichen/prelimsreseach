import os
import nltk
from nltk.tag.stanford import StanfordPOSTagger
from nltk.corpus import state_union, wordnet
from nltk.tokenize import PunktSentenceTokenizer, word_tokenize
from nltk.corpus.reader import NOUN
train_text = state_union.raw("2006-GWBush.txt")
sample_text = state_union.raw("2006-GWBush.txt")
custom_sent_tokenizer = PunktSentenceTokenizer(train_text)
tokenized = custom_sent_tokenizer.tokenize(sample_text)

java_path = "C:\\Program Files\\Java\\jdk1.8.0_20\\bin\\java.exe"
os.environ['JAVA_HOME']= java_path
def process_content():
    try:
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            print(tagged)

    except Exception as e:
        print(str(e))

#process_content()


#text = "i need a complete tutorial about Eclipse plugin. My Plugin has not a graphical interface, but I need to use his function inside another plugin or java app. I use eclipse ONLY to load this plugin, but must work in eclipse. It should be easy, but I don't know how to do this."
text = "i need a complete tutorial about Eclipse plugin"
text1 = """I want to use a track-bar to change a form's opacity.
This is my code
decimal trans = trackBar1.Value / 5000;
this.Opacity = trans;
When I try to build it, I get this error:
Cannot implicitly convert type 'decimal' to 'double'.
I tried making trans a, but then the control doesn't work. This code has worked fine for me in VB.NET in the past.
"""
text2 = "i want to use a track bar to change a form s opacity this is my code decimal trans trackbar1 value 5000 this opacity trans when i try to build it i get this error cannot implicitly convert type decimal to double i tried making trans a double but then the control does not work this code has worked fine for me in vb net in the past  "
#text = "They refuse to permit us to obtain the refuse permit"
#tokenized_text = word_tokenize(text.lower())
#print(nltk.pos_tag(tokenized_text, tagset='universal'))
#for pair in nltk.pos_tag(tokenized_text):
#    if pair[1] == 'NN':
#        print(pair[0])
print("stanford")
#for pair in nltk.pos_tag_sents(text):
#    if pair[1] == 'NN':
#        print(pair[0])
#print(nltk.pos_tag_sents(text))
stanfordPOStagger = StanfordPOSTagger("C:\\Python34\\Lib\\site-packages\\nltk\\tag\\models\\english-left3words-distsim.tagger","C:\\Python34\\Lib\\site-packages\\nltk\\tag\\stanford-postagger-3.5.2.jar")
for taggedWord in stanfordPOStagger.tag(text2.split()):
    if taggedWord[1] == 'NN' or taggedWord[1] == 'NNP':
        print(taggedWord[0])

#tokenized_text_custom = custom_sent_tokenizer.tokenize(text)
#text = word_tokenize("They refuse to permit us to obtain the refuse permit")
##new_text = nltk.pos_tag(tokenized_text)
#print(new_text)
#list_noun = []
#for item in new_text:
#    if item[1] == 'NOUN':
#        list_noun.append((item[0]))
#
#print(list_noun)
#set(list_noun)

#for word in tokenized_text:
#    print(word)
#    for syn in wordnet.synsets(word, pos=None):
#        print(syn.pos)
    #if len(synsets) == 0:
    #    print(word)
#for synset in list(wordnet.all_synsets('n')):
#    print(synset)
"""
new_text_custom = nltk.pos_tag(tokenized_text_custom, tagset='universal')
#new_text = nltk.pos_tag(tokenized_text)
print(new_text_custom)
list_noun = []
for item in new_text_custom:
    if item[1] == 'NOUN':
        list_noun.append((item[0]))

print(set(list_noun))
"""